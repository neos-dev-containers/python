# python

You should create the file structure bellow:

```text
 📁 DevEnvironment
 ├─ 📁 .devcontainer
 │  ├─ 📃 devcontainer.json
 │  └─ 📃 docker-compose.yml
 │
 └─ 📁 Workspace
    └─ 📁 YourApplication
       ├─ 📁 YourApplication
       │  ├─ 🐍 __init__.py
       │  ├─ 🐍 app.py
       │  └─ 📃 requirements.txt
       │
       ├─ 📁 tests
       │  ├─ 🐍 __init__.py
       │  └─ 🐍 app.py
       │
       └─ 📁 docs
          ├─ 📄 README.md
          └─ 📄 diagram.puml
```

The Workspace folder is a suggestion for Folder Structure for Python applications. You can use your own template.

The important files will be described bellow:

---

## .devcontainer/devcontainer.json

The extensions section is an example with suggestions for Python development. You can add or remove to meet your necessities.

```json
{
    "name": "Python & MySQL5",
    "dockerComposeFile": "docker-compose.yml",
    "service": "app",
    "workspaceFolder": "/workspace",
    "extensions":
    [
        "ms-python.python",
        "VisualStudioExptTeam.vscodeintellicode",
        "ms-python.vscode-pylance",
        "deerawan.vscode-dash",
        "oderwat.indent-rainbow",
        "CoenraadS.bracket-pair-colorizer",
        "frhtylcn.pythonsnippets",
        "LittleFoxTeam.vscode-python-test-adapter",
        "aaron-bond.better-comments",
        "njpwerner.autodocstring",
        "KevinRose.vsc-python-indent"
    ],
    "settings": {
        "terminal.integrated.shell.linux": "/bin/zsh",
        "editor.fontLigatures": true,
        "python.formatting.provider": "black"
    }
}
```

## .devcontainer/docker-compose.yml

You can use any other services related with your application or development process. The MySQL 5 is an example.

The ```app``` section name is conected with the container where you will be connected (in the ```devcontainer.json > service```). 

It's importante to notice that volume section is mapping to your Workspace folder. This is important to connect the vscode to your local environment.

If you need another python version you can choose one of the tags available: ```[3.9, 3.8, 3.7, 3.6]```

```yaml
services:
  app:
    image: registry.gitlab.com/neos-dev-containers/images/python:3.9
    volumes:
      - ../Workspace:/workspace
    command: sleep infinity

  mysql:
    image: mysql:5
    environment:
      MYSQL_ROOT_PASSWORD: "!mypass123"
      MYSQL_DATABASE: "template"
      MYSQL_USER: "myuser"
      MYSQL_PASSWORD: "mypass"
```

