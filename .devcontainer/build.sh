#!/bin/bash

docker build -t registry.gitlab.com/neos-dev-containers/images/python:3.9 --build-arg VARIANT=3.9 . 
docker build -t registry.gitlab.com/neos-dev-containers/images/python:3.8 --build-arg VARIANT=3.8 . 
docker build -t registry.gitlab.com/neos-dev-containers/images/python:3.7 --build-arg VARIANT=3.7 .
docker build -t registry.gitlab.com/neos-dev-containers/images/python:3.6 --build-arg VARIANT=3.6 .
