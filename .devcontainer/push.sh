#!/bin/bash

docker push registry.gitlab.com/neos-dev-containers/images/python:3.9
docker push registry.gitlab.com/neos-dev-containers/images/python:3.8
docker push registry.gitlab.com/neos-dev-containers/images/python:3.7
docker push registry.gitlab.com/neos-dev-containers/images/python:3.6
